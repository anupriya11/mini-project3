package com.hcl.miniproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hcl.miniproject.pojo.AdminLogin;
import com.hcl.miniproject.pojo.Items;
import com.hcl.miniproject.pojo.LoginUser;
import com.hcl.miniproject.repository.AdminLogRepository;
import com.hcl.miniproject.repository.ItemRepository;
import com.hcl.miniproject.repository.UserRepository;




@SpringBootApplication
public class MiniprojectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(MiniprojectApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("Anupriya","TV","anupriya@gmail.com","9900990099","anupriya","123456");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("Vijay","vv","Vijay@gmail.com","9999000000","Vijay","123456");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("karthi","Y","karthi@gmail.com","8888800000","kar","1234");
		userRepository.save(login3);
		
		LoginUser login4=new LoginUser("anvi","T","anvi@test.com","7777788888","anvi","anvi");
		userRepository.save(login4);
		
		AdminLogin admin1=new AdminLogin("arun", "rr", "arun@gmail.com", "9999911111", "arun", "arun");
		adminLogRepository.save(admin1);

		Items book=new Items("Veg Roll","80","3","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("Burgur","100","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("Samosa","20","1","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("South India Meal ","150","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("Parata","50","2","IMG005");
		bookRepository.save(book4);
		
		
	}


}
